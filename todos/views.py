from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView,
from todos.models import TodoList

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todo_lists/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todo_lists/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todo_lists/create.html"
